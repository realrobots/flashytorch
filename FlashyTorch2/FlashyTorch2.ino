#include <IRremote.h>
#include <EEPROM.h>

#define BUTTON1 A2
#define BUTTON2 A3
#define POT     A1

#define LED     9
#define IR      3

#define IRRECEIVER A4

bool buttonState[2];
long buttonDownTime[2];
long buttonUpTime[2];
bool isButtonHeld[2];

long lastFlash = 0;

IRsend irsend;

// IR RECEIVER
IRrecv irrecv(IRRECEIVER);
decode_results results;

bool rpt;

// Storage for the recorded code
int codeType = -1; // The type of code
unsigned long codeValue; // The code value if not raw
unsigned int rawCodes[RAWBUF]; // The durations if raw
int codeLen; // The length of the code
int toggle = 0; // The RC5/6 toggle state

bool isPress = false;

void setup() {
  // put your setup code here, to run once:
  pinMode(BUTTON1, INPUT_PULLUP);
  pinMode(BUTTON2, INPUT_PULLUP);
  pinMode(POT, INPUT);
  pinMode(LED, OUTPUT);
  pinMode(IR, OUTPUT);
  irrecv.enableIRIn(); // Start the receiver

  Serial.begin(9600);
}

void OnButton1Down() {
  if (millis() - buttonUpTime[0] > 100) {
    Serial.println("BUTTON 1 DOWN");
    buttonDownTime[0] = millis();
    buttonState[0] = 1;
    rpt = 0;

    sendCode(0);
  }
}

void OnButton1Up() {
  isButtonHeld[0] = 0;
  buttonState[0] = 0;
  buttonUpTime[0] = millis();
  Serial.println("BUTTON 1 UP");
}

void OnButton2Down() {
  if (millis() - buttonUpTime[1] > 100) {
    Serial.println("BUTTON 2 DOWN");
    buttonDownTime[1] = millis();
    buttonState[1] = 1;
    rpt = 0;

    sendCode(1);
  }
}

void OnButton2Up() {
  isButtonHeld[1] = 0;
  buttonState[1] = 0;
  buttonUpTime[1] = millis();
  Serial.println("BUTTON 2 UP");

}

void loop() {
//  digitalWrite(IR, 1);
//  delay(100);
//  digitalWrite(IR, 0);
//  delay(900);
//
//  return;
  // put your main code here, to run repeatedly:
  int potVal = 1023 - analogRead(A1);
//  Serial.println(potVal);
//  delay(20);

  if (potVal < 128) {
    digitalWrite(9, 0);

    if (!digitalRead(BUTTON1) && !buttonState[0]) {
      OnButton1Down();
    } else if (digitalRead(BUTTON1) && buttonState[0]) {
      OnButton1Up();
    }

    if (buttonState[0] && !isButtonHeld[0] && millis() - buttonDownTime[0] > 1000) {
      isButtonHeld[0] = 1;
      Serial.println("BUTTON 1 HELD");
    }

    if (!digitalRead(BUTTON2) && !buttonState[1]) {
      OnButton2Down();
    } else if (digitalRead(BUTTON2) && buttonState[1]) {
      OnButton2Up();
    }

    if (buttonState[1] && !isButtonHeld[1] && millis() - buttonDownTime[1] > 1000) {
      isButtonHeld[1] = 1;
      Serial.println("BUTTON 2 HELD");
    }

    if (irrecv.decode(&results)) {
      Serial.println(isButtonHeld[0]);
      if (isButtonHeld[0]) {
        digitalWrite(LED, 1);
        storeCode(&results, 0);
        digitalWrite(LED, 0);
      } else if (isButtonHeld[1]) {
        digitalWrite(LED, 1);
        storeCode(&results, 1);
        digitalWrite(LED, 0);
      }
      irrecv.resume(); // resume receiver
    }

  } else if (potVal < 1023 - 128) {
    int interval = map(potVal, 128, 1023 - 128, 1, 100);
    //Serial.println(interval);
    digitalWrite(9, 1);
    delay(interval);
    digitalWrite(9, 0);
    delay(interval);
  } else {
    //Serial.println("ON");
    digitalWrite(9, 1);
  }
  delay(10);

}

void ReportInputs() {
  Serial.print(analogRead(POT));
  Serial.print("\t");
  Serial.print(digitalRead(BUTTON1));
  Serial.print("\t");
  Serial.println(digitalRead(BUTTON2));
  delay(20);
}

void TestLED() {
  if (millis() - lastFlash > 1000) {
    digitalWrite(IR, 1);
    lastFlash = millis();
    delay(50);
    digitalWrite(IR, 0);
  }
}

void sendCode(int idx) {
  rpt = !rpt;
  bool repeat = rpt;
  if (idx == 0) {
    codeType = EEPROM.read(0);
    codeLen = EEPROM.read(1);
    codeValue = EEPROM_readlong(2);
  } else {
    codeType = EEPROM.read(100);
    codeLen = EEPROM.read(101);
    codeValue = EEPROM_readlong(102);
  }

  if (codeType == NEC) {
    if (repeat) {
      irsend.sendNEC(REPEAT, codeLen);
      Serial.println("Sent NEC repeat");
    }
    else {
      irsend.sendNEC(codeValue, codeLen);
      Serial.print("Sent NEC ");
      Serial.println(codeValue, HEX);
    }
  }
  else if (codeType == SONY) {
    irsend.sendSony(codeValue, codeLen);
    Serial.print("Sent Sony ");
    Serial.println(codeValue, HEX);
  }
  else if (codeType == PANASONIC) {
    irsend.sendPanasonic(codeValue, codeLen);
    Serial.print("Sent Panasonic");
    Serial.println(codeValue, HEX);
  }
  else if (codeType == JVC) {
    irsend.sendPanasonic(codeValue, codeLen);
    Serial.print("Sent JVC");
    Serial.println(codeValue, HEX);
  }
  else if (codeType == RC5 || codeType == RC6) {
    if (!repeat) {
      // Flip the toggle bit for a new button press
      toggle = 1 - toggle;
    }
    // Put the toggle bit into the code to send
    codeValue = codeValue & ~(1 << (codeLen - 1));
    codeValue = codeValue | (toggle << (codeLen - 1));
    if (codeType == RC5) {
      Serial.print("Sent RC5 ");
      Serial.println(codeValue, HEX);
      irsend.sendRC5(codeValue, codeLen);
    }
    else {
      irsend.sendRC6(codeValue, codeLen);
      Serial.print("Sent RC6 ");
      Serial.println(codeValue, HEX);
    }
  }
  else if (codeType == UNKNOWN /* i.e. raw */) {
    // Assume 38 KHz
    irsend.sendRaw(rawCodes, codeLen, 38);
    Serial.println("Sent raw");
  }

  irrecv.enableIRIn(); // Re-enable receiver
}

// Stores the code for later playback
// Most of this code is just logging
void storeCode(decode_results *results, int idx) {
  codeType = results->decode_type;
  int count = results->rawlen;


  if (codeType == UNKNOWN) {
    Serial.println("Received unknown code, saving as raw");
    codeLen = results->rawlen - 1;
    // To store raw codes:
    // Drop first value (gap)
    // Convert from ticks to microseconds
    // Tweak marks shorter, and spaces longer to cancel out IR receiver distortion
    for (int i = 1; i <= codeLen; i++) {
      if (i % 2) {
        // Mark
        rawCodes[i - 1] = results->rawbuf[i] * USECPERTICK - MARK_EXCESS;
        Serial.print(" m");
      }
      else {
        // Space
        rawCodes[i - 1] = results->rawbuf[i] * USECPERTICK + MARK_EXCESS;
        Serial.print(" s");
      }
      Serial.print(rawCodes[i - 1], DEC);
    }
    Serial.println("");
  }
  else {
    if (codeType == NEC) {
      Serial.print("Received NEC: ");
      if (results->value == REPEAT) {
        // Don't record a NEC repeat value as that's useless.
        Serial.println("repeat; ignoring.");
        return;
      }
    }
    else if (codeType == SONY) {
      Serial.print("Received SONY: ");
    }
    else if (codeType == PANASONIC) {
      Serial.print("Received PANASONIC: ");
    }
    else if (codeType == JVC) {
      Serial.print("Received JVC: ");
    }
    else if (codeType == RC5) {
      Serial.print("Received RC5: ");
    }
    else if (codeType == RC6) {
      Serial.print("Received RC6: ");
    }
    else {
      Serial.print("Unexpected codeType ");
      Serial.print(codeType, DEC);
      Serial.println("");
    }
    Serial.println(results->value, HEX);
    codeValue = results->value;
    codeLen = results->bits;
    if (idx = 0) {
      Serial.println(codeValue);
      EEPROM.write(0, codeType);
      EEPROM.write(1, codeLen);
      EEPROM_writelong(2, codeValue);
    } else if (idx = 1) {
      Serial.println(codeValue);
      EEPROM.write(100, codeType);
      EEPROM.write(101, codeLen);
      EEPROM_writelong(102, codeValue);
    }
  }
}

void EEPROM_writelong(int address, unsigned long value)
{
  //truncate upper part and write lower part into EEPROM
  EEPROM_writeint(address + 2, word(value));
  //shift upper part down
  value = value >> 16;
  //truncate and write
  EEPROM_writeint(address, word(value));
}

unsigned long EEPROM_readlong(int address)
{
  //use word read function for reading upper part
  unsigned long dword = EEPROM_readint(address);
  //shift read word up
  dword = dword << 16;
  // read lower word from EEPROM and OR it into double word
  dword = dword | EEPROM_readint(address + 2);
  return dword;
}

void EEPROM_writeint(int address, int value)
{
  EEPROM.write(address, highByte(value));
  EEPROM.write(address + 1 , lowByte(value));
}
unsigned int EEPROM_readint(int address)
{
  unsigned int word = word(EEPROM.read(address), EEPROM.read(address + 1));
  return word;
}
